var subDomainName = 'super-cookie'
var baseDomainName = 'MYDOMAIN'
var subDomainCount = 8

$(function() {                                
    getCookie()
})

function setCookie() {
    let value = parseInt($('#range').val())
    $('#btnSetCookie').hide()
    $('#btnSetCookieLoading').show()
    let cookieBits = ('00000000' + value.toString(2)).slice(-8)
    console.log('Setting bits: ' + cookieBits + ' (' + ConvertBase.bin2dec(cookieBits) + ')')
    for (let i = 0; i < subDomainCount; i++) {                    
        let bitValue = cookieBits[i]
        if (bitValue == 1) {
            var serverNum = ('00' + (i + 1)).slice(-2)
            $.ajax(
                {
                    url: 'https://' + subDomainName + serverNum + '.' + baseDomainName, 
                    type: 'GET',
                    serverNum: parseInt(serverNum),
                    success: function(result) {
                        console.log('Set bit 1 on server: ' + this.serverNum)
                    }
                }
            )
        }
    }

    setTimeout(function() {
        getCookie()
    }, 3 * 1000)    
}

// Source URL: https://gist.github.com/faisalman/4213592
var ConvertBase = function (num) {
    return {
        from : function (baseFrom) {
            return {
                to : function (baseTo) {
                    return parseInt(num, baseFrom).toString(baseTo)
                }
            }
        }
    }
}
    
// Binary to decimal
ConvertBase.bin2dec = function (num) {
    return ConvertBase(num).from(2).to(10)
}
   
this.ConvertBase = ConvertBase

function getCookie() {
    var cookieResult = []
    for (let i = 1; i <= subDomainCount; i++) {
        let serverNum = ('00' + i).slice(-2)
        $.ajax(
            {
                url: 'http://' + subDomainName + serverNum + '.' + baseDomainName, 
                type: 'GET',
                serverNum: parseInt(serverNum),
                success: function(result) {
                    if (result == 'https://') {
                        cookieResult.push(this.serverNum + ':1')
                        console.log('Read bit 1 on server: ' + this.serverNum)
                    }
                    else {
                        cookieResult.push(this.serverNum + ':0')
                        console.log('Read bit 0 on server: ' + this.serverNum)
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    cookieResult.push(this.serverNum + ':0')
                    console.log('Read bit 0 on server: ' + this.serverNum)
                }
            }
        )
    }

    var intervalObj = setInterval(function() {
        if (cookieResult.length == subDomainCount) {
            clearInterval(intervalObj)

            let isCookieSet = false
            let cookieBits = ''
            for (let serverNum = 1; serverNum <= subDomainCount; serverNum++) {
                for (let i = 0; i < subDomainCount; i++) {                
                    let cookieServerNum = parseInt(cookieResult[i].split(':')[0])
                    if (serverNum == cookieServerNum) {                                           
                        let cookieBitValue = cookieResult[i].split(':')[1]
                        console.log('Parsed bit ' + cookieBitValue + ' from server: ' + cookieServerNum)
                        cookieBits += cookieBitValue

                        if (cookieBitValue == '1') {
                            isCookieSet = true
                        }

                        break
                    }
                }                
            }

            if (!isCookieSet) {
                $('#pnlLoading').hide()
                $('#pnlEnterValue').show()

                console.log('Retrieved bits: ' + cookieBits + ' (0)')
                console.log('No cookie value is set.')
            }
            else {
                let cookieValue = ConvertBase.bin2dec(cookieBits)
                console.log('Retrieved bits: ' + cookieBits + ' (' + cookieValue + ')')

                $('#cookieValue').html(cookieValue)
                $('#pnlLoading').hide()
                $('#pnlEnterValue').hide()
                $('#pnlShowValue').show()
            }
        }
    }, 500)
}
