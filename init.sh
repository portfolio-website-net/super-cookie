#!/bin/bash

git clone https://gitlab.com/portfolio-website-net/super-cookie.git

USERNAME="portfoliowebsite"
DOMAIN="portfolio-website.net"

# Search-and-replace the username in the script file
sed -i "s/MYUSERNAME/$USERNAME/g" ./super-cookie/run-setup.sh

# Search-and-replace the domain in the script files
sed -i "s/MYDOMAIN/$DOMAIN/g" ./super-cookie/helper.js
sed -i "s/MYDOMAIN/$DOMAIN/g" ./super-cookie/run-setup.sh
sed -i "s/MYDOMAIN/$DOMAIN/g" ./super-cookie/subdomain-setup01.sh

chmod u+x ./super-cookie/run-setup.sh
./super-cookie/run-setup.sh
