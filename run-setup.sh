#!/bin/bash
set -euo pipefail

# Script based on https://github.com/do-community/automated-setups/blob/master/Ubuntu-18.04/initial_server_setup.sh

# Begin: Configure the new non-root user

########################
### SCRIPT VARIABLES ###
########################

# Name of the user to create and grant sudo privileges
USERNAME=MYUSERNAME

# Whether to copy over the root user's `authorized_keys` file to the new sudo
# user.
COPY_AUTHORIZED_KEYS_FROM_ROOT=false

# Additional public keys to add to the new sudo user
# OTHER_PUBLIC_KEYS_TO_ADD=(
#     "ssh-rsa AAAAB..."
#     "ssh-rsa AAAAB..."
# )
OTHER_PUBLIC_KEYS_TO_ADD=(
)

####################
### SCRIPT LOGIC ###
####################

# Add sudo user and grant privileges
useradd --create-home --shell "/bin/bash" --groups sudo "${USERNAME}"

# Check whether the root account has a real password set
encrypted_root_pw="$(grep root /etc/shadow | cut --delimiter=: --fields=2)"

if [ "${encrypted_root_pw}" != "*" ]; then
    # Transfer auto-generated root password to user if present
    # and lock the root account to password-based access
    echo "${USERNAME}:${encrypted_root_pw}" | chpasswd --encrypted
    passwd --lock root
else
    # Delete invalid password for user if using keys so that a new password
    # can be set without providing a previous value
    passwd --delete "${USERNAME}"
fi

# Expire the sudo user's password immediately to force a change
chage --lastday 0 "${USERNAME}"

# Create SSH directory for sudo user
home_directory="$(eval echo ~${USERNAME})"
mkdir --parents "${home_directory}/.ssh"

# Copy `authorized_keys` file from root if requested
if [ "${COPY_AUTHORIZED_KEYS_FROM_ROOT}" = true ]; then
    cp /root/.ssh/authorized_keys "${home_directory}/.ssh"
fi

# Add additional provided public keys
for pub_key in "${OTHER_PUBLIC_KEYS_TO_ADD[@]}"; do
    echo "${pub_key}" >> "${home_directory}/.ssh/authorized_keys"
done

# Adjust SSH configuration ownership and permissions
chmod 0700 "${home_directory}/.ssh"

if [ -d "${home_directory}/.ssh/authorized_keys" ]; then
chmod 0600 "${home_directory}/.ssh/authorized_keys"
fi

chown --recursive "${USERNAME}":"${USERNAME}" "${home_directory}/.ssh"

# Disable root SSH login with password
sed --in-place 's/^PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
if sshd -t -q; then
    systemctl restart sshd
fi

# End: Configure the new non-root user


# Begin: Install Nginx

apt update
apt install nginx -y

# End: Install Nginx


# Begin: Configure the firewall to allow SSH, HTTP, and HTTPS

# Add exception for SSH and Nginx Full and then enable UFW firewall
ufw allow OpenSSH
ufw allow 'Nginx Full'
ufw --force enable

# End: Configure the firewall to allow SSH, HTTP, and HTTPS


# Begin: Install PHP

apt install php-fpm -y

# End: Install PHP


# Begin: Configure the main site resources (index.html and helper.js)

mkdir -p /var/www/super-cookie/html
chown -R "${USERNAME}":"${USERNAME}" /var/www/super-cookie/html
chmod -R 755 /var/www/super-cookie

printf 'server {
        listen 80;
        listen [::]:80;

        root /var/www/super-cookie/html;
        index index.php index.html;

        server_name super-cookie.MYDOMAIN;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ \\.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        }

        location ~ /\\.ht {
            deny all;
        }
}' > /etc/nginx/sites-available/super-cookie

cp ./super-cookie/index.html /var/www/super-cookie/html/index.html
cp ./super-cookie/helper.js /var/www/super-cookie/html/helper.js

ln -s /etc/nginx/sites-available/super-cookie /etc/nginx/sites-enabled/

systemctl restart nginx

# End: Configure the main site resources (index.html and helper.js)


# Begin: Install and configure a new certificate using Let’s Encrypt

add-apt-repository ppa:certbot/certbot -y

apt install python-certbot-nginx -y

# Allow for time to assign the server's IP address to DNS
sleep 1m

certbot --nginx --non-interactive --agree-tos --domain super-cookie.MYDOMAIN --email contact@MYDOMAIN

printf 'server {
    server_name super-cookie.MYDOMAIN;

    root /var/www/super-cookie/html;
    index index.php index.html;

    location / {
            try_files $uri $uri/ =404;
    }

    location ~ \\.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }

    location ~ /\\.ht {
        deny all;
    }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/super-cookie.MYDOMAIN/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/super-cookie.MYDOMAIN/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    listen 80;
    listen [::]:80;

    root /var/www/super-cookie/html;
    index index.php index.html;

    server_name super-cookie.MYDOMAIN;

    location / {
            try_files $uri $uri/ =404;
    }

    location ~ \\.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }

    location ~ /\\.ht {
        deny all;
    }
}' > /etc/nginx/sites-available/super-cookie

systemctl restart nginx

# End: Install and configure a new certificate using Let’s Encrypt


# Begin: Run the setup process for the subdomains

for i in $(seq -f "%02g" 1 8)
do
    sed "s/01/$i/g" "./super-cookie/subdomain-setup01.sh" > "./super-cookie/setup$i.sh"
    chmod u+x "./super-cookie/setup$i.sh"
    "./super-cookie/setup$i.sh"
done

# End: Run the setup process for the subdomains


# Begin: Clean up the script files

rm -rf super-cookie

# End: Clean up the script files