#!/bin/bash

# Begin: Configure Nginx with the PHP subdomain script

mkdir -p /var/www/super-cookie01/html
chown -R "${USERNAME}":"${USERNAME}" /var/www/super-cookie01/html
chmod -R 755 /var/www/super-cookie01

printf 'server {
        listen 80;
        listen [::]:80;

        root /var/www/super-cookie01/html;
        index index.php index.html;

        server_name super-cookie01.MYDOMAIN;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ \\.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        }

        location ~ /\\.ht {
            deny all;
        }
}' > /etc/nginx/sites-available/super-cookie01

cp ./super-cookie/subdomain.php /var/www/super-cookie01/html/index.php

ln -s /etc/nginx/sites-available/super-cookie01 /etc/nginx/sites-enabled/

systemctl restart nginx

# End: Configure Nginx with the PHP subdomain script


# Begin: Install and configure a new certificate for the subdomain using Let’s Encrypt

certbot --nginx --non-interactive --agree-tos --domain super-cookie01.MYDOMAIN --email contact@MYDOMAIN

printf 'server {
    server_name super-cookie01.MYDOMAIN;

    root /var/www/super-cookie01/html;
    index index.php index.html;

    location / {
            try_files $uri $uri/ =404;
    }

    location ~ \\.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }

    location ~ /\\.ht {
        deny all;
    }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/super-cookie01.MYDOMAIN/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/super-cookie01.MYDOMAIN/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    listen 80;
    listen [::]:80;

    root /var/www/super-cookie01/html;
    index index.php index.html;

    server_name super-cookie01.MYDOMAIN;

    location / {
            try_files $uri $uri/ =404;
    }

    location ~ \\.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }

    location ~ /\\.ht {
        deny all;
    }
}' > /etc/nginx/sites-available/super-cookie01


systemctl restart nginx

# End: Install and configure a new certificate for the subdomain using Let’s Encrypt