<?php
    // Set the ‘Strict-Transport-Security’ header for 10 minutes
    header('Strict-Transport-Security: max-age=600');

    // Set the Cross-Origin Resource Sharing (CORS) headers to allow external requests from ‘helper.js’
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: X-Requested-With');

    // Source URL: https://stackoverflow.com/a/14270161
    if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        
        $protocol = 'https://';
    }
    else {
        $protocol = 'http://';
    }

    // Return ‘http://’ for HTTP requests and ‘https://’ for HTTPS requests
    echo $protocol;
?>